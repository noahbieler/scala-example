# Scala Base Project

This repo contains the basic scaffold to create a Scala program.

## Installing prerequisits

You can either use Maven or `sbt`, the Scala build tool, which can be installed
according to the instructions 
[here](http://www.scala-sbt.org/0.13/docs/Installing-sbt-on-Linux.html).

Also make sure to install Scala itself (best is version 2.11).

## Running

To try the example, run 

    sbt run

or if you prefere Maven

    mvn install
    scala -cp target/App-1.0-SNAPSHOT.jar org.example.App

