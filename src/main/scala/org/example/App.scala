package org.example

object App {

  /**
    * Show difference between mutable and immutable programming
    */
  def main(args: Array[String]): Unit = {
    println("Hello World")
    println(s"You typed ${args.mkString(" ")}")
  }
}
